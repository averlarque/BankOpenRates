import urllib.request
import csv
import io
from tkinter import Tk, StringVar, Label, Frame
from datetime import datetime
import ssl


def metal_rate_check():
    # url with csv file
    url = '''Replace with an URL to csv file with metals rate''' 
    # opening and reading the csv-file
    context = ssl._create_unverified_context()  # url is not verified so we avoid warnings
    # opening fle from url
    f = urllib.request.urlopen(url, context=context)
    # reading and decoding as string
    fl = f.read().decode('utf-8')
    # closing connection
    f.close()
    # streaming the data as strings
    fio = io.StringIO(fl, newline="")
    # parsing strings as csv file format
    cr = csv.reader(fio)
    # for storing the required rates as a list
    rates = []
    # parsing the output
    for row in cr:
        line = row[0].split(';')
        if line[0] == '000-02':
            rates.append(line[5])
        else:
            break
    # results as a dictionary    
    rates_dict = {
        'Pd': (rates[0], rates[0 + 4]),
        'Pt': (rates[1], rates[1 + 4]),
        'Au': (rates[2], rates[2 + 4]),
        'Ag': (rates[3], rates[3 + 4])
    }
    return rates_dict


def currency_rate_check():
    # url with csv file
    url = '''Replace with an URL to csv file with currency rates'''
    # url is not verified so we avoid warnings
    context = ssl._create_unverified_context()
    # opening fle from url
    f = urllib.request.urlopen(url, context=context)
    # reading and decoding as string
    fl = f.read().decode('utf-8')
    # closing connection
    f.close()
    # streaming the data as strings
    fio = io.StringIO(fl, newline="")
    # parsing strings as csv file format
    cr = csv.reader(fio)
    # for storing the required rates as a dictionary
    dct = {}
    for row in cr:
        line = row[0].split(';')
        # if it corresponds Moscow region
        if not line[0] == '000-68':
            continue
        # latest items from Moscow region
        elif line[4] == '1':
            # distributing values by using a currency title as a key
            if line[3] not in dct.keys():
                dct[line[3]] = [line[5]]
            elif line[3] in dct.keys():
                dct[line[3]].append(line[5])
    # checking difference, fewer rate should be first as a rate of selling this currency
    for rate in dct.keys():
        if float(dct[rate][0]) > float(dct[rate][1]):
            higher = dct[rate][0]
            lower = dct[rate][1]
            dct[rate] = [lower, higher]
        else:
            continue
    return dct


def rates_gui():
    # extracting current metal rates
    metal_rates = metal_rate_check()
    currency_rate = currency_rate_check()
    # time of the check
    time = str(datetime.now())[:19]
    # GUI module
    root = Tk()
    root.resizable(width=False, height=False)
    root.title('Openbank rates')

    current_time = StringVar()
    Label(root, textvariable=current_time, relief='groove').grid(columnspan=2, row=0, column=0)
    current_time.set('Время проверки ' + time)

    # currency rates
    usd = StringVar()
    Label(root, textvariable=usd).grid(row=1, column=0)
    usd.set('USD: ' + str(currency_rate['USD'][0] + ' ' + str(currency_rate['USD'][1])))

    eur = StringVar()
    Label(root, textvariable=eur).grid(row=2, column=0)
    eur.set('EUR: ' + str(currency_rate['EUR'][0] + ' ' + str(currency_rate['EUR'][1])))

    gbp = StringVar()
    Label(root, textvariable=gbp).grid(row=3, column=0)
    gbp.set('GBP: ' + str(currency_rate['GBP'][0] + ' ' + str(currency_rate['GBP'][1])))

    chf = StringVar()
    Label(root, textvariable=chf).grid(row=4, column=0)
    chf.set('CHF: ' + str(currency_rate['CHF'][0] + ' ' + str(currency_rate['CHF'][1])))

    # metal rates
    gold = StringVar()
    Label(root, textvariable=gold).grid(row=1, column=1)
    gold.set('Золото: ' + str(metal_rates['Au'][0]) + ' ' + str(metal_rates['Au'][1]))

    pd = StringVar()
    Label(root, textvariable=pd).grid(row=2, column=1)
    pd.set('Палладий: ' + str(metal_rates['Pd'][0]) + ' ' + str(metal_rates['Pd'][1]))

    pt = StringVar()
    Label(root, textvariable=pt).grid(row=3, column=1)
    pt.set('Платина: ' + str(metal_rates['Pt'][0]) + ' ' + str(metal_rates['Pt'][1]))

    silver = StringVar()
    Label(root, textvariable=silver).grid(row=4, column=1)
    silver.set('Серебро: ' + str(metal_rates['Ag'][0]) + ' ' + str(metal_rates['Ag'][1]))

    root.mainloop()

if __name__ == '__main__':
    rates_gui()

