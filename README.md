Simple script designed for checking exel-tables with currencies and metals rates from a specific bank in order to check the correctness of the rates on bank's
website.
It's written on Python 3.5 only with usage of standard library. GUI is implemented via Tkinter.

For implemented any changes download the repo, make changes, state url for tables and complile an exe-file.

Compiled to exe-file with pyinstaller:

pip install pyinstaller

pyinstaller --onefile rate_checker.py <name of exe-file>